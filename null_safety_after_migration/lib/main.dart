import 'package:flutter/material.dart';
import 'package:null_safety_after_migration/pages/home_page.dart';

void main() {
  runApp(const MyApp(Key("New App")));
}

class MyApp extends StatefulWidget {
  const MyApp(Key key) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ramal Karir',
      theme: ThemeData(
        primarySwatch: createMaterialColor(const Color(0XFFE3562A)),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: const TextTheme(
          button: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
          headline4: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            letterSpacing: -0.5,
            color: Color(0xFF3C3A36),
          ),
          bodyText2: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color(0XFF78746D),
          ),
        ),
      ),
      home: const HomePage(),
    );
  }
}

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map<int, Color> swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int index = 1; index < 10; index++) {
    strengths.add(0.1 * index);
  }

  for (double strength in strengths as Iterable<double>) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }

  return MaterialColor(color.value, swatch);
}
