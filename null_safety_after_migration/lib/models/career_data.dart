class CareerData {
  String careerName;
  String careerImage;
  String careerDescription;

  CareerData({
    required this.careerName,
    required this.careerImage,
    required this.careerDescription,
  });
}
