class UserData {
  String name;

  String favouriteColor;

  int favouriteNumber;

  UserData({
    required this.name,
    required this.favouriteColor,
    required this.favouriteNumber,
  });

  int get favouriteColorValue {
    int colorValue = favouriteColor.codeUnits.fold(0, (a, b) => a + b);
    return colorValue;
  }

  int get favouriteNumberValue {
    int favouriteNumberValue =
        ((favouriteNumber * 2) + (favouriteNumber * 0.15 * favouriteNumber * 2))
            .toInt();
    return favouriteNumberValue;
  }

  int get careerValue {
    int careerValue = favouriteColorValue + favouriteNumberValue;
    return careerValue;
  }
}
