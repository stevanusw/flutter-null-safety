import 'package:flutter/material.dart';
import 'package:null_safety_before_migration/models/career_data.dart';
import 'package:null_safety_before_migration/pages/home_page.dart';

class ResultPage extends StatefulWidget {
  final CareerData careerData;

  const ResultPage({
    Key key,
    this.careerData,
  }) : super(key: key);

  @override
  _ResultPageState createState() => _ResultPageState();
}

class _ResultPageState extends State<ResultPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: _buildAppBar(context),
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildCareerImage(context),
                _buildCareerName(context),
                _buildCareerDescription(context),
                _buildTryAgainButton(context)
              ]),
        )),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      centerTitle: true,
      title: Text(
        "Hasil Kamu",
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }

  Image _buildCareerImage(BuildContext context) {
    return Image.asset(
      widget.careerData.careerImage,
      width: MediaQuery.of(context).size.width,
    );
  }

  Text _buildCareerName(BuildContext context) {
    return Text(
      widget.careerData.careerName,
      style: Theme.of(context).textTheme.headline4,
    );
  }

  Padding _buildCareerDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 32),
      child: Text(
        widget.careerData.careerDescription,
        style: Theme.of(context).textTheme.bodyText2,
        textAlign: TextAlign.center,
      ),
    );
  }

  SizedBox _buildTryAgainButton(BuildContext context) {
    return SizedBox(
      height: 56,
      width: MediaQuery.of(context).size.width,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          backgroundColor: Theme.of(context).primaryColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                12,
              ),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => const HomePage(),
            ),
          );
        },
        child: Text(
          "Coba Lagi!",
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
  }
}
