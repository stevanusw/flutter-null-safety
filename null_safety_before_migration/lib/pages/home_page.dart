import 'package:flutter/material.dart';
import 'package:null_safety_before_migration/models/career_predictor.dart';
import 'package:null_safety_before_migration/models/user_data.dart';
import 'package:null_safety_before_migration/pages/result_page.dart';
import 'package:progress_indicator_button/progress_button.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nameTextEditingController;
  TextEditingController colorTextEditingController;
  TextEditingController numberTextEditingController;

  @override
  void initState() {
    nameTextEditingController = TextEditingController();
    colorTextEditingController = TextEditingController();
    numberTextEditingController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: _buildAppBar(context),
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(
                top: 20,
                left: 32,
                right: 32,
                bottom: 30,
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildHomeImage(context),
                    _buildNameTextField(),
                    _buildFavouriteColorTextField(),
                    _buildFavouriteNumberTextField(),
                    _buildPredictButton(context)
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      centerTitle: true,
      title: Text(
        "Ramal Karir",
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }

  Padding _buildHomeImage(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 32),
      child: Image.asset(
        "assets/images/home.png",
        width: MediaQuery.of(context).size.width - 150,
      ),
    );
  }

  TextFormField _buildNameTextField() {
    return TextFormField(
      controller: nameTextEditingController,
      decoration: const InputDecoration(
        labelText: "Nama",
        hintText: "Masukkan nama kamu",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
      ),
    );
  }

  Padding _buildFavouriteColorTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: TextFormField(
        controller: colorTextEditingController,
        decoration: const InputDecoration(
          labelText: "Warna Favorit",
          hintText: "Masukkan warna favorit kamu",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
          ),
        ),
      ),
    );
  }

  Padding _buildFavouriteNumberTextField() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 32),
      child: TextFormField(
        controller: numberTextEditingController,
        decoration: const InputDecoration(
          labelText: "Angka Favorit",
          hintText: "Masukkan angka favorit kamu (1-99)",
          counterText: "",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
          ),
        ),
        maxLength: 2,
        keyboardType: TextInputType.number,
      ),
    );
  }

  SizedBox _buildPredictButton(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 56,
      child: ProgressButton(
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: (AnimationController controller) async {
          if (controller.isCompleted) {
            controller.reverse();
          } else {
            controller.forward();
          }

          await Future.delayed(Duration(seconds: 1));
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => ResultPage(
                careerData: CareerPredictor(
                  userData: UserData(
                    name: nameTextEditingController.text,
                    favouriteColor: colorTextEditingController.text,
                    favouriteNumber: int.tryParse(
                      numberTextEditingController.text,
                    ),
                  ),
                ).careerPredictions,
              ),
            ),
          );
        },
        child: Text(
          "Ramal Karir Saya!",
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
  }
}
