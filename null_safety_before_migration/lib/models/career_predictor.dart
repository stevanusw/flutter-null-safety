import 'package:null_safety_before_migration/models/career_data.dart';
import 'package:null_safety_before_migration/models/user_data.dart';

class CareerPredictor {
  UserData userData;
  CareerPredictor({this.userData});

  CareerData get careerPredictions {
    String valueInString = userData.careerValue.toString();
    int lastDigit = int.tryParse(valueInString[valueInString.length - 1]);
    if (lastDigit == 0 || lastDigit == 1 || lastDigit == 3) {
      return CareerData(
        careerName: "Bocah Petualang",
        careerImage: "assets/images/career1.png",
        careerDescription:
            "${userData.name} si ${userData.name} si bocah petualang... Kuat kakinya seperti kaki kijang, hap hap hap hap hap. Hujan dan panas bukan halangan untuk dirimu, melawan binatang buas mulai dari ayam hingga kecoak terbang dapat kamu taklukkan. Selamat, kamu cocok menjadi si bocah petualang!",
      );
    } else if (lastDigit == 2 || lastDigit == 4) {
      return CareerData(
        careerName: "Tukang Sate",
        careerImage: "assets/images/career2.png",
        careerDescription:
            "Sateeee..... Sateee.... Selamat kamu ternyata mempunyai bakat dalam memasak!!! Khususnya memasak nasi, lontong, dan juga sate!!!! Selain itu kamu juga sangat lihai dalam meracik bumbu terutama menggunakan sambal, kecap, acar dan kacang. Karena itu kamu cocok menjadi tukang sate!!!",
      );
    } else if (lastDigit == 7 || lastDigit == 8) {
      return CareerData(
        careerName: "Trader Profesional",
        careerImage: "assets/images/career3.png",
        careerDescription:
            "Jutaan orang yang tak mengenal dirimu dan juga dirimu sendiri tidak menyadari bahwa kamu mempunyai bakat terpendam dalam melihat layar monitor. Selain itu, jual & beli adalah keahlian utamamu. Selamat, kamu cocok menjadi seorang trader profesional!",
      );
    } else {
      return CareerData(
        careerName: "Pawang Hujan",
        careerImage: "assets/images/career4.png",
        careerDescription:
            "Air adalah sahabat kamu dari kecil. Mulai dari minum, berenang hingga ke kamar mandi pun kamu pasti bareng sama air. Karena itu, profesi yang cocok untukmu adalah Pawang Hujan! Kamu dipanggil mulai dari petani yang butuh hujan maupun oleh Blackpink saat konser di tempat terbuka biar ga ada hujan.",
      );
    }
  }
}
