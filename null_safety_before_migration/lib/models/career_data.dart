class CareerData {
  String careerName;
  String careerImage;
  String careerDescription;

  CareerData({
    this.careerName,
    this.careerImage,
    this.careerDescription,
  });
}
