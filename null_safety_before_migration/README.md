# null_safety_before_migration

This is example project for null safety implementation before migrated to null safety.

### Prerequisites

1. To run the project please follow the setup and installation for flutter in here: https://flutter.dev/docs/get-started/install
2. You can install visual studio code IDE from here: https://code.visualstudio.com/download
3. Also this is the extension that can be used to improve the experiences using Flutter using visual studio code IDE in here: https://medium.com/flutter-community/must-have-vs-code-extensions-for-working-with-flutter-e31a421b9c68

### Installing

1. Pull from this git
2. Run `flutter pub get`
3. Run `flutter run` to run the application

## Built With Framework

* [Flutter](https://flutter.dev/)

## Authors

**Stevanus Wijaya**